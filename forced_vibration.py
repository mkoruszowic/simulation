"""
Numerical simulation of forced vibration
Author: Michał Koruszowic
Engineering Physics
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as patches
import matplotlib.patches as mpatches
from matplotlib.widgets import Slider,Button
from matplotlib.gridspec import GridSpec

#parameters
k = 100 #spring constant [N/m]
r = 2 #coefficient of damping force [kg/s]
F = 3 #applied force [N]
m = 5 #mass [kg]
omega = np.sqrt(k/m) #angular frequency [rad/s]
h = 1e-3 # [s]
x_0 = 0 #initial position [m]
v_0 = 0 #initial speed [m/s]
t_0 = 0 #time [s]
tmax = 20 #limit of x axis

"""~~~~~~~~~~~~~~~~~~~~~~~~~~~Setting parameters~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""

while 1:
    print('Current parameters: k = ', k ,', r = ' ,r, ', F = ',F ,', m = ',m,', omega = ',round(omega,2), ', x_0 = ',x_0, ', v_0 = ', v_0,'.\n')
    print('Available operations: \n')
    print('(1) Choose new parameters')
    print('(2) Leave parameters')
    while 1:
        try:
            n=int(input("Select an action: "))
            break
        except:
            print("\nPlease enter a number from the range 1-2!")
    print("\n")

    while n>=3: #zabezpieczenie w przypadku podania liczby spoza zakresu
        while 1:
            try:
                n=int(input("Number out of range. Select an action: "))
                break
            except:
                print("\nPlease enter a number!")
    if n == 1:
        while 1:
            try:
                k = float(input("k = "))
                r = float(input("r = "))
                F = float(input("F = "))
                m = float(input("m = "))
                omega = float(input("omega = "))
                x_0 = float(input("x_0 = "))
                v_0 = float(input("v_0 = "))
                break
            except:
                print("\nPlease enter a number!")
        break  
    elif n == 2:
        break

"""~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""

def update(val):
    """Updates values"""
    global k, r, F, m, omega
    k = k_wsp.val 
    r = r_wsp.val
    F = F_wsp.val
    m = m_wsp.val
    omega = m_wsp.val
    
def reset(event):
    """Reset to the initial value"""
    k_wsp.reset()
    r_wsp.reset()
    F_wsp.reset()
    m_wsp.reset()
    omega_wsp.reset()

def a(x,v,t):
    """Returns current acceleration"""
    return (- k * x - r * v + F * np.sin(omega * t)) / m

def animate(frame):
    """Creates animation by updating values"""
    global x, v, t 
    
    #Euler method
    acc = a(x,v,t)
    x += v * h + 1/2 * acc * h**2
    v += acc * h   
    t += h
  
    x_list.append(x)
    t_list.append(t)
    
    #creating line
    linex = [-4, x]
    liney = [0, 0]
    
    rect.set_x(x)
    
    line.set_data(linex,liney)
      
    chart.set_data(t_list, x_list)
    
    if t >= tmax - 1:
        chart.axes.set_xlim(t - tmax + 1.0, t + 1.0) 
    
    #axes autoscaling
    l_min, l_max = chart.axes.get_ylim()
    ymax = max(x_list) 
    ymin = min(x_list)
    
    if x >= l_max or x <= l_min :
        
        rect.set_width((-ymin + ymax) / 3 )
        line.axes.set_xlim(1.5 * (ymin - rect.get_width()) ,1.5 * (ymax + 2*rect.get_width()))
        chart.axes.set_ylim(1.5*ymin ,1.5* ymax )
        
    return line, rect, chart,#unpacking

"""~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Axes limits~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""

x_list, t_list = [], []
x = x_0
v = v_0
t = t_0

#setting initial axis limits
for i in range(7000):
    acc = a(x,v,t)
    x += v * h + 1/2 * acc * h**2
    v += acc * h   
    t += h

    
    x_list.append(x)
    
ymax = max(x_list)
ymin = min(x_list)
x = x_0
v = v_0
t = t_0

"""~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Plots~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""

fig = plt.figure(figsize = [12, 5], num=1) 
fig.suptitle('Simulation of forced vibrations', fontsize = 16)
gs = GridSpec(2, 2, figure=fig)

rect = patches.Rectangle((0, -0.5), (-ymin + ymax) / 3 , 1, facecolor = '#654321')

#creating subplots
ax1 = fig.add_subplot(gs[0,0], xlim = (1.5 * (ymin - rect.get_width()) ,1.5 * (ymax + 2*rect.get_width())), ylim = (-3,3)  ,xlabel='x(t)')
ax2 = fig.add_subplot(gs[:,1],  xlim = (0, tmax), ylim = (1.5*ymin ,1.5*ymax )  ,xlabel='t', ylabel='x(t), m')

plt.subplots_adjust(wspace = 0.3)

ax1.add_patch(rect) 
x_list, t_list = [], []

line, = ax1.plot([], [], ls = '-', color = '#808080')
line.axes.get_yaxis().set_ticks([])                 
chart, = ax2.plot([], [], ls = '-', color = 'b') 
chart.axes.get_xaxis().set_ticks([])

#sliders
slader_k = plt.axes([0.125, 0.37, 0.33, 0.03]) 
k_wsp = Slider(slader_k, 'k', 1, 300, valfmt='%.f', valinit = k, valstep= 1)

slader_r = plt.axes([0.125, 0.31, 0.33, 0.03])
r_wsp = Slider(slader_r, 'r', 0, 100, valfmt='%.f', valinit = r, valstep= 1)

slader_F = plt.axes([0.125, 0.25, 0.33, 0.03]) 
F_wsp = Slider(slader_F, 'F', 0, 100, valfmt='%.f', valinit = F, valstep= 1)

slader_m = plt.axes([0.125, 0.19, 0.33, 0.03]) 
m_wsp = Slider(slader_m, 'm', 1, 100, valfmt='%.f', valinit = m, valstep= 1)

slader_omega = plt.axes([0.125, 0.13, 0.33, 0.03]) 
omega_wsp = Slider(slader_omega, '\u03A9', 0, 50, valfmt='%.f', valinit = omega, valstep = 1)

#reset button
resetax = plt.axes([0.125, 0.07, 0.1, 0.04])
button = Button(resetax, 'Reset')


k_wsp.on_changed(update) 
r_wsp.on_changed(update)
F_wsp.on_changed(update)
m_wsp.on_changed(update)
omega_wsp.on_changed(update)
button.on_clicked(reset)

#handlers
mass = mpatches.Patch(color= rect.get_facecolor(), label='Block')
ln = mpatches.Patch(color= line.get_color(), label='Spring')
graph = mpatches.Patch(color= chart.get_color(), label=' Position')

ax1.legend( handles = [ln, mass] )
ax2.legend(handles = [graph])

#making animation
ani = animation.FuncAnimation(fig, animate, blit= True, interval = 1)

plt.show()
"""~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"""